﻿using System.Collections.Generic;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str)
        {
            string rts = "";
            char[] arr = str.ToCharArray();


            for (int i = str.Length - 1; i >= 0 ; i--)
            {
                rts +=  arr[i];
            }

            return rts;
        }

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 1; i <= exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node)
        {
            string str = node.Text;

            Tree tree = (Tree)node;

            foreach (Node majorbranch in tree.Children)
            {
                str += "-" + majorbranch.Text;

                Tree MajBrnch = (Tree)majorbranch;

                foreach (Node minorbranch in MajBrnch.Children)
                {
                    str += "-" + minorbranch.Text;
                    try {//bypass the Nodes that aren't also Tree types
                        Tree MinBrnch = (Tree)minorbranch;

                        foreach (Node leaf in MinBrnch.Children)
                        {
                            str += "-" + leaf.Text;
                        }
                    }
                    catch
                    {
                        continue;
                    }    

                }
            }
            return str;
        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
